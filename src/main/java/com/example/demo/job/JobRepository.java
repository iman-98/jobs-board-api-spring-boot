package com.example.demo.job;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface JobRepository extends PagingAndSortingRepository<Job, Long> {
    Job findJobById(Long id);
    Page<Job> findAll(Pageable pageable);
    Page<Job> findAllByStatusAndUserId(Boolean status, Long userId, Pageable pageable);
    Page<Job> findByUserId(Long userId, Pageable pageable);
    Page<Job> findAllByStatus(boolean status, Pageable paging);
}
