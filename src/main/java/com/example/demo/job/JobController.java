package com.example.demo.job;

import com.example.demo.user.models.User;
import com.example.demo.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/job")
public class JobController {
    private final JobService jobService;
    private final UserRepository userRepository;
    private final JobRepository jobRepository;

    @Autowired
    public JobController(JobService jobService, UserRepository userRepository, JobRepository jobRepository) {
        this.jobService = jobService;
        this.userRepository = userRepository;
        this.jobRepository = jobRepository;
    }


    @PostMapping("/save")
    @PreAuthorize("hasRole('RECRUITER') or hasRole('ADMIN')")
    public ResponseEntity<Object> saveJob(@RequestBody Job job) {
        String username = User.currentUserName();
        Optional<User> userOptional = userRepository.findByUsername(username);
        if (userOptional.isEmpty()) return ResponseEntity.notFound().build();
        User user = userOptional.get();
        job.setUser(user);
        jobService.saveJob(job);
        return new ResponseEntity<>(job, HttpStatus.CREATED);
    }

    @GetMapping("/list")
    public Page<Job> getJobs(
            @RequestParam(required = false) Boolean status,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        Pageable paging = PageRequest.of(page, size);
        return jobService.findBy(status, paging);
    }


    @PostMapping(path = "/updateStatus/{id}")
    @PreAuthorize("hasRole('RECRUITER') or hasRole('ADMIN')")
    public ResponseEntity<Object> updateJobStatus(@PathVariable Long id, @RequestBody Boolean status) {
        String username = User.currentUserName();
        Optional<User> userOptional = userRepository.findByUsername(username);
        if (userOptional.isEmpty())
            return ResponseEntity.notFound().build();
        Long userId = userOptional.get().getId();
        Job job = jobRepository.findJobById(id);
        if (Objects.equals(job.getUser().getId(), userId)) {
            job.setStatus(status);
            jobRepository.save(job);
        } else
            return new ResponseEntity<>("Not Allowed!", HttpStatus.UNAUTHORIZED);

        return ResponseEntity.ok("Status Updated Successfully!");
    }
}
