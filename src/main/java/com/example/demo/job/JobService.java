package com.example.demo.job;

import com.example.demo.user.models.User;
import com.example.demo.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class JobService {

    private final JobRepository jobRepository;
    private final UserRepository userRepository;

    @Autowired
    public JobService(JobRepository jobRepository, UserRepository userRepository) {
        this.jobRepository = jobRepository;
        this.userRepository = userRepository;
    }

    public void saveJob(Job job) {
        jobRepository.save(job);
    }

    public Page<Job> findBy(Boolean status, Pageable paging) {
        Page<Job> publicJobPosts = jobRepository.findAllByStatus(true, paging);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = User.currentUserName();
        Optional<User> userOptional = userRepository.findByUsername(username);
        if (userOptional.isEmpty()) return publicJobPosts;
        Long id = userOptional.get().getId();
        boolean hasRecruiterRole = authentication.getAuthorities().stream().anyMatch(r -> r.getAuthority().equals("ROLE_RECRUITER"));
        if (hasRecruiterRole) {
            if (status == null) return jobRepository.findByUserId(id, paging);
            else return jobRepository.findAllByStatusAndUserId(status, id, paging);
        } else return publicJobPosts;
    }
}
