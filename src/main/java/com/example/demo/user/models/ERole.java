package com.example.demo.user.models;

public enum ERole {
    ROLE_USER,
    ROLE_RECRUITER,
    ROLE_ADMIN,
}
