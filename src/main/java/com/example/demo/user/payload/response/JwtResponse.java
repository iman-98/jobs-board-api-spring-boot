package com.example.demo.user.payload.response;

import java.util.List;

public class JwtResponse {
    private String jwt;
    private Long id;
    private String username;
    private String email;
    private String roles;

    public JwtResponse(String jwt, Long id, String username, String email, List<String> roles) {
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }
}