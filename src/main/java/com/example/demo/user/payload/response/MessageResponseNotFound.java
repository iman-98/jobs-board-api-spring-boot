package com.example.demo.user.payload.response;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public
class MessageResponseNotFound extends RuntimeException {
    public MessageResponseNotFound(String message) {
        super(message);
    }
}

