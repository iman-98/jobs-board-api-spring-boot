package com.example.demo.application;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationRepository extends PagingAndSortingRepository<Application, Long> {
    Application findApplicationById(Long id);
    Page<Application> findAll(Pageable pageable);
    Page<Application> findAllBySeenAndJobId(Boolean status, Long userId, Pageable pageable);
    Page<Application> findByJobId(Long jobId, Pageable pageable);
}