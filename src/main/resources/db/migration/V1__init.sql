CREATE TABLE users
(
    id       SERIAL PRIMARY KEY,
    username varchar(20),
    email    varchar(50),
    password varchar(120)
);

CREATE TABLE roles
(
    id   SERIAL PRIMARY KEY,
    name varchar(20)
);

INSERT INTO roles(name)
VALUES ('ROLE_USER');
INSERT INTO roles(name)
VALUES ('ROLE_RECRUITER');
INSERT INTO roles(name)
VALUES ('ROLE_ADMIN');

CREATE TABLE user_roles
(
    id      SERIAL PRIMARY KEY,
    user_id bigint unsigned,
    role_id bigint unsigned
);

CREATE TABLE application
(
    id      SERIAL PRIMARY KEY,
    user_id bigint unsigned,
    job_id  bigint unsigned,
    seen    bit
);

CREATE TABLE job
(
    id          SERIAL PRIMARY KEY,
    title       varchar(255),
    description varchar(800),
    status      bit,
    user_id     bigint unsigned
);

ALTER TABLE user_roles ADD CONSTRAINT  foreign key (user_id) references users(id);
ALTER TABLE user_roles ADD CONSTRAINT  foreign key (role_id) references roles(id);
ALTER TABLE application ADD CONSTRAINT  foreign key (user_id) references users(id);
ALTER TABLE application ADD CONSTRAINT  foreign key (job_id) references job(id);
ALTER TABLE job ADD CONSTRAINT  foreign key (user_id) references users(id);

