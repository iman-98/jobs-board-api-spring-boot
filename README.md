# Jobs Boards API #

### Java Spring Boot ###

## Goal ##

The task is to build a simple jobs board API using: [Java Spring Boot/ Spring Rest](https://spring.io/projects/spring-boot). 
The goal of the task is to demonstrate coding and analysis skills.

## Scope ##

* The scope of work only includes the backend / API. 
* No front end work is required.
* Authentication is based on [JWT](https://jwt.io/)
* Endpoints should be accessible only with JWT tokens
* Endpoints should be secured using Roles
* Services and endpoints should be unit-tested and well covered 


## Techincal Requirements ##

* Should use Java > v11
* Should only use **Spring Boot / Rest** > v2.4, other frameworks are not accepted.
* Should use spring-boot-starter-security for authorization
* Should use [Flyway](https://github.com/flyway/flyway) or [Liquibase](https://www.liquibase.org) for database migrations.
* Communication should be JSON
* Authentication should use JWT tokens
* HTTP responses should follow [REST](https://restfulapi.net) Guidelines
* Should use a relational database  / Sqlite3, (please include the file in the code)
* A postman file should be included to demonstrate the functionality


## Description ##

Types of users:

* Recruiters
* Applicants


## Discussion ##

***Recruiter***: 

Recruiter is the party that has an available job-vacancy, a recruiter can:

* Register a new recruiter account
* Login to get a JWT token
* Create a job post
* Update a job post
	* change job-post status (open/closed)
* List job posts
	* list his jobs-vacancies
	* filter by job-vacancies's status, open/closed
	* paginate, (page-index,size)
* List job-applicants / job-applications
	* list all
	* filter by job-application's status (seen / not seen)
	* paginate, (page-index, size)
* Security:
	* can only edit his own job-vacancies

***Applicant*** 

Applicant is the party looking for a job. Applicant can:

* Register a new user
* Login to get a JWT token
* Apply to an open vacancy
* Security:
	* can only list open job-vacancies
	* can only apply to open vacancies

***Job***

Job is the vacancy the recruiter offers. A Job:

* has the following attributes:
	* title
	* description 
	* status (open / closed) 

	
***Job Application***:

Job application represents the the applicant's interest in the job offered. Job application has the following attributes:

* on creation, the job-application is set as not-seen
* job-application is set to seen after the recruiter retrieves the job-application at least once.



## Testing requirements ##
* Code should be tested using the standard junit
* all endpoints should be tested
* the more tests the better



## Review Process ##

### Code quality
Is your code well-structured? Do you keep your coding style consistent across your codebase

### Security
How do you store your customers' passwords? What about security of your customers' data? how you are securing the API endpoints


### Testability
Is your code tested? How do you approach testing? Do you use TDD or are tests an afterthought for you.


### API structure and usability
How do you structure API endpoints? Do you follow REST principles? Do you make use of proper response codes and HTTP headers where it makes sense?


### Validations and error handling
How do you handle required fields, and errors that might appear due to invalid data, How do you handle responses and Exceptions



## Project Delivery

Source code delivery options:

- Create a Fork of this repository on Bitbucket and then create a pull request back to it
- Create a private repository on bitbucket and invite devteam@boundlessdrop.com




	

